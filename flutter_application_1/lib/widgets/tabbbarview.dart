import 'package:flutter/material.dart';
import 'package:flutter_application_1/widgets/home_page.dart';

class TabItem {
  //переключение страниц с главной на 2е и 3е
  String title;
  Icon icon;
  TabItem({this.title, this.icon});
}

final List<TabItem> _tabBar = [
  //создание списков табов и иконок к ним
  TabItem(title: "Photo", icon: Icon(Icons.photo)),
  TabItem(title: "Chat", icon: Icon(Icons.message)),
  TabItem(title: "Album", icon: Icon(Icons.album)),
];

class BottomNavigationBarTest extends StatefulWidget {
  BottomNavigationBarTest({Key key}) : super(key: key);
  @override
  _BottomNavigationBarState createState() => _BottomNavigationBarState();
}

class _BottomNavigationBarState extends State<BottomNavigationBarTest>
    with TickerProviderStateMixin {
  //для передачи This
  TabController _tabController;
  int _currentTabIndex = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(
        length: _tabBar.length,
        vsync: this); // для передачи This создается тикерпровайдермиксин
    _tabController.addListener(() {
      // листенер для перехода свайпами и отображения перехода на табах
      print('Listener: ${_tabController.index}');
      setState(() {
        _currentTabIndex = _tabController.index;
      });
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: TabBarView(
        controller: _tabController,
        children: [
          HomePage(), //главный экран
          Container(
            color: Colors.blue,
            child: Center(child: Text("второй")), //второй экран
          ),
          Container(
            color: Colors.red,
            child: Center(child: Text("третий")), //третий экран
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
          onTap: (index) {
            setState(() {
              _tabController.index = index;
              _currentTabIndex = index;
            });
          },
          currentIndex: _currentTabIndex,
          items: [
            for (final item in _tabBar)
              BottomNavigationBarItem(label: item.title, icon: item.icon)
          ]),
    );
  }
}
