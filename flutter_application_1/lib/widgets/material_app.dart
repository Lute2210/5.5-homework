import 'package:flutter/material.dart';
import 'tabbbarview.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(primaryColor: Colors.grey[500]),
      home: BottomNavigationBarTest(), //основное тело приложение
    );
  }
}
