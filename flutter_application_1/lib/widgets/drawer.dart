import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MyDrawer extends StatelessWidget {
  //правое выпадающее меню главного экрана
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Column(
              children: [
                DrawerHeader(
                  child: CircleAvatar(
                      radius: 70,
                      backgroundImage: AssetImage("assets/images/image1.png")),
                ),
                ListTile(
                  leading: Icon(Icons.home),
                  title: Text("Home"),
                  trailing: Icon(Icons.arrow_right),
                  onTap: () {
                    print("ShortTap");
                  },
                  onLongPress: () {
                    print("LongTap");
                  },
                ),
                ListTile(
                  leading: Icon(Icons.portrait),
                  title: Text("Profile"),
                  trailing: Icon(Icons.arrow_right),
                  onTap: () {
                    print("ShortTap");
                  },
                  onLongPress: () {
                    print("LongTap");
                  },
                ),
                ListTile(
                  leading: Icon(Icons.image),
                  title: Text("Images"),
                  trailing: Icon(Icons.arrow_right),
                  onTap: () {
                    print("ShortTap");
                  },
                  onLongPress: () {
                    print("LongTap");
                  },
                ),
              ],
            ),
          ),
          Container(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ElevatedButton(onPressed: () {}, child: Text("Выход")),
                  ElevatedButton(onPressed: () {}, child: Text("Регистрация"))
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
