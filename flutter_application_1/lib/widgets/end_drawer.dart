import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MyEndDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.grey[700],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            DrawerHeader(
              child: CircleAvatar(
                radius: 80,
                backgroundImage: AssetImage("assets/images/image3.png"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
