import 'package:flutter/material.dart';
import 'package:flutter_application_1/widgets/drawer.dart';
import 'end_drawer.dart';

class HomePage extends StatefulWidget {
  //первая страница приложения

  HomePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  void _showMModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return Container(
            height: 200,
            color: Colors.blue[200],
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Row(
                        children: [
                          Icon(Icons.wallet_giftcard),
                          Padding(padding: EdgeInsets.all(5)),
                          Text("Сумма")
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(10),
                      child: Text("200 руб."),
                    )
                  ],
                ),
                ElevatedButton(onPressed: () {}, child: Text("Оплатить"))
              ],
            ),
          );
        });
  }

  void openDrawer() {
    scaffoldKey.currentState
        .openEndDrawer(); //для вызова левой панели с другой иконки
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Flutter Demo Home Page"),
        centerTitle: true,
        actions: [
          Builder(
              builder: (context) => IconButton(
                  icon: Icon(Icons.person),
                  onPressed: () {
                    Scaffold.of(context)
                        .openEndDrawer(); //вызов левой панели с другой иконки
                  })),
        ],
      ),
      drawer: MyDrawer(), //правая скользящая панель
      endDrawer: MyEndDrawer(),
      floatingActionButton: FloatingActionButton.extended(
        label: Text("Press"),
        backgroundColor: Colors.grey[900],
        onPressed: () {
          _showMModalBottomSheet(context); //вызов BottomSheet
        },
      ),
    );
  }
}
